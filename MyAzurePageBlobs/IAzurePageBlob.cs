using System.IO;
using System.Threading.Tasks;

namespace MyAzurePageBlobs
{
    public interface IAzurePageBlob
    {
        ValueTask<long> GetBlobSizeAsync();
        ValueTask ReadAsync(Stream stream, long pageFrom, long pages);
        
        ValueTask ReadAsync(byte[] dest, int arrayStartIndex, long pageFrom, long pages);
        
        ValueTask DownloadAsync(Stream stream);
        ValueTask WriteAsync(Stream data, long pageNo, int resizeRatio=1);

        ValueTask CreateIfNotExists();

        ValueTask<bool> ExistsAsync();

        ValueTask ResizeBlobAsync(long blobSize);


        ValueTask DeleteAsync();

    }


    public static class AzurePageBlobExtensions
    {
        public static async ValueTask<MemoryStream> ReadAsync(this IAzurePageBlob azurePageBlob, long pageFrom, long pages)
        {
            var memStream = new MemoryStream();
            await azurePageBlob.ReadAsync(memStream, pageFrom, pages);
            memStream.Position = 0;
            return memStream;
        }


        public static async ValueTask<MemoryStream> DownloadAsync(this IAzurePageBlob azurePageBlob)
        {
            var memStream = new MemoryStream();
            await azurePageBlob.DownloadAsync(memStream);
            memStream.Position = 0;
            return memStream;
        }
    }
}