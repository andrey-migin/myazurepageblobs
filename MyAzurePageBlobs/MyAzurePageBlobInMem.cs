using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DotNetCoreDecorators;

namespace MyAzurePageBlobs
{
    public class MyAzurePageBlobInMem : IAzurePageBlob
    {
        public class PageInMem
        {
            public PageInMem()
            {
                Data = new byte[MyAzurePageBlobUtils.PageSize];
            }

            public PageInMem(byte[] src)
            {
                if (src.Length != MyAzurePageBlobUtils.PageSize)
                    throw new Exception("Page data must have exactly " + MyAzurePageBlobUtils.PageSize + " bytes");

                Data = src;
            }


            public readonly byte[] Data;

        }



        private readonly List<PageInMem> _mem = new List<PageInMem>();

        
        private long GetBlobSize()
        {
           return _mem.Count * MyAzurePageBlobUtils.PageSize;
        }

        public ValueTask<long> GetBlobSizeAsync()
        {
            return new ValueTask<long>(GetBlobSize());
        }

        public ValueTask ReadAsync(Stream stream, long pageFrom, long pages)
        {
            for (var i = 0; i < pages; i++)
            {
                stream.Write(_mem[(int)pageFrom + i].Data);
            }

            return new ValueTask();
        }

        public async ValueTask ReadAsync(byte[] destArray, int arrayStartIndex, long pageFrom, long pages)
        {
            var memStream = new MemoryStream();
            await ReadAsync(memStream, pageFrom, pages);

            var array = memStream.ToArray();
            Array.Copy(array, 0, destArray, arrayStartIndex, array.Length);
        }


        public ValueTask DownloadAsync(Stream stream)
        {

            foreach (var b in _mem)
                stream.Write(b.Data);

            return new ValueTask();
        }

        private static byte[] ToArray(Stream src)
        {
            switch (src)
            {
                case MemoryStream memoryStream:
                    return memoryStream.ToArray();
            }

            var resultStream = new MemoryStream();
            src.Position = 0;
            src.CopyTo(resultStream);
            return resultStream.ToArray();
        }

        public ValueTask WriteAsync(Stream data, long pageNo, int resizeRatio)
        {
            if (data.Length % MyAzurePageBlobUtils.PageSize != 0)
                throw new Exception("Page must be rounded to " + MyAzurePageBlobUtils.PageSize);
            
            var requiredBlobSize = MyAzurePageBlobUtils.CalculateRequiredBlobSize(pageNo, data.Length, resizeRatio);


            var currentBlobSize = GetBlobSize();
            
            if (currentBlobSize < requiredBlobSize)
                ResizeBlob(requiredBlobSize);

            foreach (var chunk in ToArray(data).SplitToChunks(512))
            {
                var newPage = new PageInMem(chunk.ToArray());
                
                _mem[(int)pageNo] = newPage;

                pageNo++;
            }

            return new ValueTask();
        }

        public ValueTask CreateIfNotExists()
        {
            return new ValueTask();
        }

        public ValueTask<bool> ExistsAsync()
        {
            return new ValueTask<bool>();
        }

        private void ResizeBlob(long blobSize)
        {
            if (GetBlobSize() < blobSize)
            {
                while (GetBlobSize() < blobSize)
                {
                    _mem.Add(new PageInMem());
                }
            }
            else
            {
                while (GetBlobSize() > blobSize)
                {
                    _mem.RemoveAt(_mem.Count-1);
                }  
            }
        }

        public ValueTask ResizeBlobAsync(long blobSize)
        {
            ResizeBlob(blobSize);
            return new ValueTask();
        }

        public ValueTask DeleteAsync()
        {
            return new ValueTask();
        }
    }
}

