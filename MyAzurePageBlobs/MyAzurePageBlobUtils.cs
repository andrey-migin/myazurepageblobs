using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace MyAzurePageBlobs
{
    public static class MyAzurePageBlobUtils
    {

        public const int PageSize = 512;

        public static long GetBlobPageNo(long offset)
        {
            return offset / PageSize;
        }

        private static readonly byte[] EmptyPage = new byte[PageSize];

        public static ReadOnlySpan<byte> GetEmptyPageWithZeros()
        {
            return EmptyPage.AsSpan();
        }


        public static bool PageSizeToWriteIsCompliantWithPageBlob(long pageSizeToWrite)
        {
            return pageSizeToWrite % PageSize == 0;
        }

        public static void MakeMemoryStreamReadyForPageBlobWriteOperation(this MemoryStream data, byte byteToFillTheEnd = 0)
        {
            if (PageSizeToWriteIsCompliantWithPageBlob(data.Position))
                return;

            var newDataLen = (int)CalculatePageSizeToWrite(data.Position);


            if (data.Length < newDataLen)
                data.SetLength(newDataLen);    
            

            var bytesToAdd = (int)(newDataLen - data.Position);
           
            var buffer = data.GetBuffer();

            var endOfTheBuffer = buffer.AsSpan((int) data.Position, bytesToAdd);
            
            endOfTheBuffer.Fill(byteToFillTheEnd);
        }


        
        public static async ValueTask<long> GetPagesAmountAsync(this IAzurePageBlob pageBlob)
        {
            var blobSize = await pageBlob.GetBlobSizeAsync();
            return blobSize / PageSize;
        }

        public static async IAsyncEnumerable<MemoryStream> ReadBlockByBlockAsync(this IAzurePageBlob pageBlob,
            int pagesInBlock)
        {

            var pagesRemain = await pageBlob.GetPagesAmountAsync();

            long pageFrom = 0;

            while (pagesRemain > 0)
            {
                var pagesToRead = pagesRemain < pagesInBlock ? pagesRemain : pagesInBlock;

                var memChunk = await pageBlob.ReadAsync(pageFrom, pagesToRead);

                yield return memChunk;

                pageFrom += pagesToRead;
                pagesRemain -= pagesToRead;
            }

        }

        public static long CalculateRequiredPagesAmount(long dataSize)
        {
            return (dataSize - 1) / PageSize + 1;
        }

        public static long CalculatePageSizeToWrite(long dataSize)
        {
            return CalculateRequiredPagesAmount(dataSize)  * PageSize;
        }

        public static long CalculateRequiredBlobSize(long pageNo, long dataLength, int resizeRatio)
        {
            var dataSizeAfterOperation = pageNo * PageSize + dataLength;
            var pageRatio = PageSize * resizeRatio;
            var requiredPages = (dataSizeAfterOperation - 1) / pageRatio+1;
            return requiredPages * pageRatio;
        }

        public static Func<int, byte[]> AllocateMemory = size => new byte[size];
    }
}