using System;
using System.IO;
using System.Threading.Tasks;

namespace MyAzurePageBlobs
{

    
    public struct WriteBytesOptions
    {
        /// <summary>
        /// If we have to increase blob size - how big it should be. 1 - means one 512 bytes page.
        /// </summary>
        public int BlobResizeRation { get; set; }
        
        
        /// <summary>
        /// If we have too big payLoad - we must split writes into chunks.
        /// 1 - means we do each write operation no more than one page = 512 bytes.
        /// 0 - means no split. 
        /// </summary>
        public int SplitRoundTripsPageSize { get; set; }
        
        
        public byte FillingWithByte { get; set; }
        
    }
    
    
    public static class MyAzurePageBlobExtensions
    {

        private static readonly WriteBytesOptions DefaultWriteOptions = new WriteBytesOptions
        {
            BlobResizeRation = 1
        };

        public static int GetBlobResizeRation(this in WriteBytesOptions writeBytesOptions)
        {
            return writeBytesOptions.BlobResizeRation < 1 ? 1 : writeBytesOptions.BlobResizeRation;
        }
        
        public static ValueTask WriteBytesAsync(this IAzurePageBlob azurePageBlob, ReadOnlyMemory<byte> data,
            long startPageId)
        {
            return azurePageBlob.WriteBytesAsync(data, startPageId, DefaultWriteOptions);
        }
        
        public static async ValueTask WriteBytesAsync(this IAzurePageBlob azurePageBlob, ReadOnlyMemory<byte> data,
            long startPageId, WriteBytesOptions writeBytesOptions)
        {

            var bufferSize = MyAzurePageBlobUtils.CalculatePageSizeToWrite(data.Length);

            if (writeBytesOptions.SplitRoundTripsPageSize == 0)
            {
                await using var memoryStream = new MemoryStream((int)bufferSize);
                memoryStream.Write(data.Span);
                memoryStream.MakeMemoryStreamReadyForPageBlobWriteOperation(writeBytesOptions.FillingWithByte);
                await azurePageBlob.WriteAsync(memoryStream, startPageId, writeBytesOptions.GetBlobResizeRation());
                return;
            }

            await azurePageBlob.SplitAndWriteAsync(data, startPageId, writeBytesOptions);

        }

        private static async ValueTask SplitAndWriteAsync(this IAzurePageBlob azurePageBlob, ReadOnlyMemory<byte> data,
            long startPageId, WriteBytesOptions writeBytesOptions)
        {

            var maxWriteChunk = writeBytesOptions.SplitRoundTripsPageSize * MyAzurePageBlobUtils.PageSize;
            var stream = new MemoryStream(maxWriteChunk);

            var remainsToWrite = data.Length;
            var pos = 0;
            
            
            while (remainsToWrite>0)
            {
                var writeChunk = remainsToWrite > maxWriteChunk ? maxWriteChunk : remainsToWrite;

                stream.Position = 0;
                stream.Write(data.Span.Slice(pos, writeChunk));

                if (!MyAzurePageBlobUtils.PageSizeToWriteIsCompliantWithPageBlob(writeChunk))
                {
                    stream.MakeMemoryStreamReadyForPageBlobWriteOperation();
                }
  
                stream.Position = 0;  

                await azurePageBlob.WriteAsync(stream, startPageId, writeBytesOptions.GetBlobResizeRation());

                remainsToWrite -= writeChunk;
                pos += writeChunk;
                startPageId += writeBytesOptions.SplitRoundTripsPageSize;
            }
            
        }


        public static async ValueTask<ReadOnlyMemory<byte>> DownloadAsReadOnlyMemoryAsync(this IAzurePageBlob azurePageBlob)
        {
            await using var stream = await azurePageBlob.DownloadAsync();

            var buffer = stream.GetBuffer();
            return new ReadOnlyMemory<byte>(buffer, 0, (int)stream.Length);
        }

    }
}