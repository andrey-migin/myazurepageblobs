using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace MyAzurePageBlobs
{
    public class MyAzurePageBlob : IAzurePageBlob
    {
        private readonly CloudStorageAccount _cloudStorageAccount;

        private readonly string _containerName;
        private readonly string _fileName;


        private CloudPageBlob _pageBlob;

        public MyAzurePageBlob(CloudStorageAccount cloudStorageAccount, string containerName, string fileName)
        {
            _cloudStorageAccount = cloudStorageAccount;
            _containerName = containerName;
            _fileName = fileName;
            CreateCloudBlobContainerAsync().Wait();
        }

        private async Task CreateCloudBlobContainerAsync()
        {
            var blobClient = _cloudStorageAccount.CreateCloudBlobClient();
            var cloudBlobContainer = blobClient.GetContainerReference(_containerName);

            await cloudBlobContainer.CreateIfNotExistsAsync();
            _pageBlob = cloudBlobContainer.GetPageBlobReference(_fileName);

        }


        private long _blobSize = -1;

        private async Task<long> GetBlobSizeFromStorageAsync()
        {

            await _pageBlob.FetchAttributesAsync();
            _blobSize = _pageBlob.Properties.Length;
            return _blobSize;
        }

        public ValueTask<long> GetBlobSizeAsync()
        {

            if (_blobSize < 0)
                return new ValueTask<long>(GetBlobSizeFromStorageAsync());

            return new ValueTask<long>(_blobSize);
        }

        public async ValueTask ResizeBlobAsync(long blobSize)
        {
            await _pageBlob.ResizeAsync(blobSize);
            _blobSize = blobSize;
        }

        public async ValueTask ReadAsync(Stream stream, long pageFrom, long pages)
        {
            await _pageBlob.DownloadRangeToStreamAsync(stream, pageFrom * MyAzurePageBlobUtils.PageSize, pages * MyAzurePageBlobUtils.PageSize);
        }

        public async ValueTask ReadAsync(byte[] dest, int arrayStartIndex, long pageFrom, long pages)
        {
            await _pageBlob.DownloadRangeToByteArrayAsync(dest, arrayStartIndex, pageFrom * MyAzurePageBlobUtils.PageSize, pages * MyAzurePageBlobUtils.PageSize);
        }

        public async ValueTask DownloadAsync(Stream stream)
        {
            await _pageBlob.DownloadToStreamAsync(stream);
        }



        public async ValueTask WriteAsync(Stream data, long pageNo, int resizeRatio)
        {
            var requiredBlobSize = MyAzurePageBlobUtils.CalculateRequiredBlobSize(pageNo, data.Length, resizeRatio);

            var blobSize = await GetBlobSizeAsync();

            if (requiredBlobSize > blobSize)
            {
                await ResizeBlobAsync(requiredBlobSize);
                Console.WriteLine($"Ressizing blob {_containerName}/{_fileName} to new size: " + requiredBlobSize);
            }

            var mt5 = MD5.Create();

            data.Position = 0;
            var hash = mt5.ComputeHash(data);
            data.Position = 0;

            try
            {
                await _pageBlob.WritePagesAsync(data, pageNo * MyAzurePageBlobUtils.PageSize, Convert.ToBase64String(hash));
            }
            catch (Exception e)
            {
                Console.WriteLine($"Attempt to write page {pageNo} is failed. Container: {_containerName}/{_fileName} DataSize: " + data.Length+"; Message: "+e.Message);
                throw;
            }
        }

        public async ValueTask CreateIfNotExists()
        {
            if (!await _pageBlob.ExistsAsync())
                await _pageBlob.CreateAsync(0);
        }

        public async ValueTask<bool> ExistsAsync()
        {
            return await _pageBlob.ExistsAsync();
        }


        public ValueTask DeleteAsync()
        {
            return new ValueTask(_pageBlob.DeleteAsync());
        }
    }
}