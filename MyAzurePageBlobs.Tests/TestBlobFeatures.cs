using System;
using System.Threading.Tasks;
using NUnit.Framework;

namespace MyAzurePageBlobs.Tests
{
    public class TestBlobExtensions
    {

        [SetUp]
        public void Init()
        {
            MyAzurePageBlobUtils.AllocateMemory = size => GC.AllocateUninitializedArray<byte>(size);
        }
        
        
        [Test]
        public async Task TestBasicBlobWritingWithResizeRatio1()
        {

            var src = new byte[500];

            for (var i = 0; i < src.Length; i++)
                src[i] = (byte) i;

            var blobInMem = new MyAzurePageBlobInMem();

            await blobInMem.WriteBytesAsync(src, 0);

            Assert.AreEqual(512, blobInMem.GetBlobSizeAsync().Result);

        }


        [Test]
        public async Task TestBasicBlobWritingWithResizeRatio2()
        {

            var src = new byte[500];

            for (var i = 0; i < src.Length; i++)
                src[i] = (byte) i;

            var blobInMem = new MyAzurePageBlobInMem();

            await blobInMem.WriteBytesAsync(src, 0,new WriteBytesOptions
            {
                BlobResizeRation = 2
            });

            Assert.AreEqual(1024, blobInMem.GetBlobSizeAsync().Result);
        }

        [Test]
        public async Task TestRandomWritingReading()
        {

        
            var src = new byte[500];

            for (var i = 0; i < src.Length; i++)
                src[i] = (byte) i;

            var blobInMem = new MyAzurePageBlobInMem();

            await blobInMem.WriteBytesAsync(src, 2,new WriteBytesOptions
            {
                BlobResizeRation = 2
            });

            Assert.AreEqual(2048, blobInMem.GetBlobSizeAsync().Result);
            
        }

        [Test]
        public async Task TestWritingOnePage()
        {

            var page = new byte[MyAzurePageBlobUtils.PageSize];

            var blobInMem = new MyAzurePageBlobInMem();

            await blobInMem.WriteBytesAsync(page, 0);

            var result = await blobInMem.DownloadAsync();
            
            Assert.AreEqual(MyAzurePageBlobUtils.PageSize, result.Length);

        }
        
    }

}