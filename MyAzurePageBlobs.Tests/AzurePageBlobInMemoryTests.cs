using System.Threading.Tasks;
using NUnit.Framework;

namespace MyAzurePageBlobs.Tests
{
    public class AzurePageBlobInMemoryTests
    {


        [Test]
        public async Task TestSizeAllocation()
        {

            var azureBlobInMemory = new MyAzurePageBlobInMem();

            await azureBlobInMemory.WriteBytesAsync(new byte[] {1, 2, 3}, 0);

            var resultArray = await azureBlobInMemory.DownloadAsync();
            
            Assert.AreEqual(512, resultArray.Length);
            
            
            await azureBlobInMemory.WriteBytesAsync(new byte[] {1, 2, 3}, 1);

            resultArray = await azureBlobInMemory.DownloadAsync();
            
            Assert.AreEqual(1024, resultArray.Length);
            
            
            await azureBlobInMemory.WriteBytesAsync(new byte[] {1, 2, 3}, 0);

            resultArray = await azureBlobInMemory.DownloadAsync();
            
            Assert.AreEqual(1024, resultArray.Length);



        }
    }
}