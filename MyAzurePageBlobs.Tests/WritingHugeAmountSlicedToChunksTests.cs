using System.Collections.Generic;
using System.Threading.Tasks;
using NUnit.Framework;

namespace MyAzurePageBlobs.Tests
{
    public class WritingHugeAmountSlicedToChunksTests
    {

        [Test]
        public async Task TestSimpleTestCase()
        {

            var dataToWrite = new List<byte>();

            for (var i = 0; i < MyAzurePageBlobUtils.PageSize; i++)
                dataToWrite.Add(1);
            
            dataToWrite.Add(2);
            dataToWrite.Add(3);
            dataToWrite.Add(4);

            var arrayToWrite = dataToWrite.ToArray();

            var myAzurePageBlob = new MyAzurePageBlobInMem();

            await myAzurePageBlob.WriteBytesAsync(arrayToWrite, 0, new WriteBytesOptions
            {
                SplitRoundTripsPageSize = 1
            });


            var resultData = await myAzurePageBlob.DownloadAsReadOnlyMemoryAsync();
            
            
            Assert.AreEqual(1024, resultData.Length);
            
            
            for (var i = 0; i < MyAzurePageBlobUtils.PageSize; i++)
                Assert.AreEqual(1, resultData.Span[i]);

            Assert.AreEqual(2, resultData.Span[512]);
            Assert.AreEqual(3, resultData.Span[513]);
            Assert.AreEqual(4, resultData.Span[514]);
            Assert.AreEqual(0, resultData.Span[515]);

        }
    }
}