using System;
using System.IO;
using NUnit.Framework;

namespace MyAzurePageBlobs.Tests
{
    public static class TestUtils
    {
        
        public static MemoryStream ToMemoryStream(this byte[] src)
        {
            var result = new MemoryStream();
            result.Write(src);
            result.Position = 0;
            return result;
        }


        public static void EqualsWith(this byte[] src, byte[] dest)
        {
            Assert.AreEqual(src.Length, dest.Length);
            for (var i = 0; i < src.Length; i++)
                Assert.AreEqual(src[i], dest[i]);
        }

        public static byte[] CreateArrayOfByte(byte b, int size)
        {
            var result = new byte[size];
            for (var i = 0; i < size; i++)
            {
                result[i] = b;
            }

            return result;
        }


        public static void FillWith(this Memory<byte> mem, byte b)
        {
            var span = mem.Span;
            for (var i = 0; i < mem.Length; i++)
                span[i] = b;
        }
        
    }
}