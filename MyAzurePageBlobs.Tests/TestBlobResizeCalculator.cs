using NUnit.Framework;

namespace MyAzurePageBlobs.Tests
{
    public class TestBlobResizeCalculator
    {

        [Test]
        public void TestResizeCase()
        {
            var result = MyAzurePageBlobUtils.CalculateRequiredBlobSize(0, 512, 1);
            Assert.AreEqual(512, result);
        }
        
        [Test]
        public void TestResizeCase2()
        {
            var result = MyAzurePageBlobUtils.CalculateRequiredBlobSize(0, 512, 2);
            Assert.AreEqual(1024, result);
        }


        [Test]
        public void TestResizeCase3()
        {
            var result = MyAzurePageBlobUtils.CalculateRequiredBlobSize(1, 512, 2);
            Assert.AreEqual(1024, result);
        }

        [Test]
        public void TestCalculateDataSize511()
        {

            var result = MyAzurePageBlobUtils.CalculateRequiredPagesAmount(511);
            Assert.AreEqual(1, result);
        }
        
        [Test]
        public void TestCalculateDataSize512()
        {

            var result = MyAzurePageBlobUtils.CalculateRequiredPagesAmount(512);
            Assert.AreEqual(1, result);
        }
        
        [Test]
        public void TestCalculateDataSize513()
        {

            var result = MyAzurePageBlobUtils.CalculateRequiredPagesAmount(513);
            Assert.AreEqual(2, result);
        }
        
        
        [Test]
        public void TestCalculateDataSize1023()
        {

            var result = MyAzurePageBlobUtils.CalculateRequiredPagesAmount(1023);
            Assert.AreEqual(2, result);
        }
        
        [Test]
        public void TestCalculateDataSize1024()
        {

            var result = MyAzurePageBlobUtils.CalculateRequiredPagesAmount(1024);
            Assert.AreEqual(2, result);
        }
        
        [Test]
        public void TestCalculateDataSize1025()
        {
            var result = MyAzurePageBlobUtils.CalculateRequiredPagesAmount(1025);
            Assert.AreEqual(3, result);
        }
        
        
    }
}